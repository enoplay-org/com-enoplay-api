package web

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

// OurOwnContextKeyType to prevent potential key collisions with 3rd party libraries
type OurOwnContextKeyType string

const UserUIDContextKey OurOwnContextKeyType = "userUID"
const AccessTokenContextKey OurOwnContextKeyType = "accessSessionToken"
const RefreshTokenContextKey OurOwnContextKeyType = "refreshSessionToken"

// EnsureAuthorization ensures a User has been authorized to access this resource
func (handler *Handler) EnsureAuthorization(h http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		authHeader := strings.Split(req.Header.Get("Authorization"), " ")
		var authSchema string
		var bearerToken string
		if len(authHeader) == 2 {
			authSchema = authHeader[0]
			bearerToken = authHeader[1]
		} else {
			handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires Authorization: Invalid Bearer Header")
			return
		}

		if authSchema != "Bearer" {
			handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires Authorization: Invalid Bearer Header")
			return
		}

		if bearerToken == "" {
			handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires Authorization: Invalid Bearer Header")
			return
		}

		userUID, err := handler.SessionTokenInteractor.VerifyAccess(bearerToken)
		if err != nil {
			handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires Authorization: Invalid Bearer Header")
			return
		}

		userCtx := context.WithValue(req.Context(), UserUIDContextKey, userUID)
		req = req.WithContext(userCtx)

		sessionCtx := context.WithValue(req.Context(), AccessTokenContextKey, bearerToken)
		req = req.WithContext(sessionCtx)

		h.ServeHTTP(res, req)
	})
}

// CheckAuthorization checks if a User has authority to access the current resource
func (handler *Handler) CheckAuthorization(h http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		authHeader := strings.Split(req.Header.Get("Authorization"), " ")
		var authSchema string
		var bearerToken string
		if len(authHeader) == 2 {
			authSchema = authHeader[0]
			bearerToken = authHeader[1]
		}

		var userUID string
		if authSchema == "Bearer" {
			userUID, _ = handler.SessionTokenInteractor.VerifyAccess(bearerToken)
		}

		userCtx := context.WithValue(req.Context(), UserUIDContextKey, userUID)
		req = req.WithContext(userCtx)

		sessionCtx := context.WithValue(req.Context(), AccessTokenContextKey, bearerToken)
		req = req.WithContext(sessionCtx)

		h.ServeHTTP(res, req)
	})
}

// EnsureAuthentication enforces that a User has been identified
func (handler *Handler) EnsureAuthentication(h http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		authHeader := strings.Split(req.Header.Get("Authorization"), " ")
		var authSchema string
		var basicToken string
		if len(authHeader) == 2 {
			authSchema = authHeader[0]
			basicToken = authHeader[1]
		} else {
			handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires Authorization: Invalid Basic Header")
			return
		}

		if authSchema != "Basic" {
			handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires Authorization: Invalid Basic Header")
			return
		}

		if basicToken == "" {
			handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires Authorization: Invalid Basic Header")
			return
		}

		userUID, err := handler.SessionTokenInteractor.VerifyRefresh(basicToken)
		if err != nil {
			handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires Authorization: Invalid Basic Header")
			return
		}

		userCtx := context.WithValue(req.Context(), UserUIDContextKey, userUID)
		req = req.WithContext(userCtx)

		sessionCtx := context.WithValue(req.Context(), RefreshTokenContextKey, basicToken)
		req = req.WithContext(sessionCtx)

		h.ServeHTTP(res, req)
	})
}

// CheckAuthentication checks if a User has been identified
func (handler *Handler) CheckAuthentication(h http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		authHeader := strings.Split(req.Header.Get("Authorization"), " ")
		var authSchema string
		var basicToken string
		if len(authHeader) == 2 {
			authSchema = authHeader[0]
			basicToken = authHeader[1]
		}

		var userUID string
		if authSchema == "Basic" {
			userUID, _ = handler.SessionTokenInteractor.VerifyRefresh(basicToken)
		}

		userCtx := context.WithValue(req.Context(), UserUIDContextKey, userUID)
		req = req.WithContext(userCtx)

		sessionCtx := context.WithValue(req.Context(), RefreshTokenContextKey, basicToken)
		req = req.WithContext(sessionCtx)

		h.ServeHTTP(res, req)
	})
}

func (handler *Handler) ForceHTTPS(res http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	if req.Header.Get("X-Forwarded-Proto") == "http" {
		urlStr := fmt.Sprintf("https://%v%v", req.Host, req.RequestURI)
		http.Redirect(res, req, urlStr, http.StatusMovedPermanently)
		return
	}
	next(res, req)
}

func (handler *Handler) CORS(res http.ResponseWriter, req *http.Request, next http.HandlerFunc) {

	res.Header().Set("Access-Control-Allow-Credentials", "true")
	res.Header().Set("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS")
	res.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Access-Control-Allow-Headers, Access-Control-Allow-Credentials, Authorization")

	if origin := req.Header.Get("Origin"); origin != "" {
		res.Header().Set("Access-Control-Allow-Origin", origin)
	} else {
		res.Header().Set("Access-Control-Allow-Origin", "*")
	}

	if req.Method == "OPTIONS" {
		res.Write([]byte{1})
		return
	}

	next(res, req)
}

var ipBlockList map[string]bool

func (handler *Handler) initIPBlockList() {
	currentDir, err := os.Getwd()
	if err != nil {
		log.Fatal(fmt.Errorf("Error retrieving current directory"))
	}

	ipBlockListBytes, err := ioutil.ReadFile(filepath.Join(currentDir, "./ip_block_list.json"))
	if err != nil {
		ipBlockListBytes, err = ioutil.ReadFile(filepath.Join(currentDir, "./interfaces/web/ip_block_list.json"))
		if err != nil {
			log.Fatal(fmt.Errorf("Error loading ip_block_list: %v", err))
		}
	}

	err = json.Unmarshal(ipBlockListBytes, &ipBlockList)
	if err != nil {
		log.Fatal(fmt.Errorf("Error parsing ip_block_list: %v", err))
	}
}

func (handler *Handler) BlockIPList(res http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	ip := req.Header.Get("X-Forwarded-For")

	// Ensure IP isn't blocked
	if !ipBlockList[ip] {
		next(res, req)
		return
	}

	// Losers...
	res.Write([]byte{0})
}
