package web

import (
	"encoding/json"
	"net/http"
)

type HandlerOptions struct {
	WwwHostURL    string `json:"wwwHostURL"`
	MobileHostURL string `json:"mobileHostURL"`
}

// Handler is a web handler.
// A web handler has methods for responding to web (HTTP/HTTPS) requests.
type Handler struct {
	options *HandlerOptions
	// util provides convenience methods to Handler
	util *Utility

	// SessionTokenInteractor is used to process sessionToken requests
	SessionTokenInteractor SessionTokenInteractor

	// PlayTokenInteractor is used to process playToken requests
	PlayTokenInteractor PlayTokenInteractor

	// UserInteractor is used to process user requests
	UserInteractor UserInteractor

	// GameInteractor is used to process game requests
	GameInteractor GameInteractor
}

// Utility has convenience methods for a Handler method to use.
type Utility struct {
	renderer  *Renderer
	recaptcha RecaptchaHandler
}

// RecaptchaHandler verifies a recaptcha string
type RecaptchaHandler interface {
	Verify(recaptcha string) bool
}

// NewHandler returns a new web Handler
func NewHandler(options *HandlerOptions, recaptchaHandler RecaptchaHandler) *Handler {
	util := Utility{}
	util.renderer = NewRenderer(RendererOptions{
		IndentJSON: true,
	}, JSON)
	util.recaptcha = recaptchaHandler
	webHandler := &Handler{}
	webHandler.options = options
	webHandler.util = &util

	webHandler.initIPBlockList()
	return webHandler
}

// DecodeRequestBody sets the value of the target object to the request body
func (util *Utility) DecodeRequestBody(req *http.Request, target interface{}) error {
	decoder := json.NewDecoder(req.Body)
	return decoder.Decode(target)
}
