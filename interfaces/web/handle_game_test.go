package web_test

import (
	"context"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"strings"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "gitlab.com/enoplay/com-enoplay-api/interfaces/web"
)

var _ = Describe("HandleGame", func() {
	Describe("Update Game Privacy", func() {
		It("should updat1e the privacy of a Game", func() {
			data := url.Values{}
			data.Add("privacy", "public")
			req, err := http.NewRequest("POST", "/games/testGID/privacy", strings.NewReader(data.Encode()))
			req.Header.Add("Content-Type", "application/json")
			req.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))
			userCtx := context.WithValue(req.Context(), UserUIDContextKey, testUserUID)
			req = req.WithContext(userCtx)
			Expect(err).NotTo(HaveOccurred())
			res := httptest.NewRecorder()

			webHandler.UpdateGamePrivacy(res, req)
			Expect(true).To(Equal(true))
			// Expect(res.Code).To(Equal(http.StatusOK))
		})
	})
})
