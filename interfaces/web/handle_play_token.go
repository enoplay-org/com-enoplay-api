package web

import (
	"fmt"
	"net/http"

	"gitlab.com/enoplay/com-enoplay-api/domain"
)

type VerifyPlayTokenRequestV0 struct {
	Token string `json:"token"`
	GID   string `json:"gid"`
}

type VerifyPlayTokenResponseV0 struct {
	User    domain.User `json:"user"`
	IsAnon  bool        `json:"isAnon"`
	Message string      `json:"message,omitempty"`
	Success bool        `json:"success,omitempty"`
}

type CreatePlayTokenRequestV0 struct {
	Username string `json:"username"`
	GID      string `json:"gid"`
}

type CreatePlayTokenResponseV0 struct {
	PlayToken string `json:"playToken"`
	Message   string `json:"message,omitempty"`
	Success   bool   `json:"success,omitempty"`
}

type CreateAnonPlayTokenRequestV0 struct {
	GID string `json:"gid"`
}

type CreateAnonPlayTokenResponseV0 struct {
	PlayToken string `json:"playToken"`
	Message   string `json:"message,omitempty"`
	Success   bool   `json:"success,omitempty"`
}

type PlayTokenInteractor interface {
	Verify(token, gameUID string) (domain.User, bool, error)
	Create(user domain.User, game domain.Game) (string, error)
	CreateAnon(game domain.Game) (string, error)
}

func (handler *Handler) VerifyPlayToken(res http.ResponseWriter, req *http.Request) {
	var body VerifyPlayTokenRequestV0
	err := handler.util.DecodeRequestBody(req, &body)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnprocessableEntity, fmt.Sprintf("Request body parse error: %v", err.Error()))
		return
	}

	game, err := handler.GameInteractor.GetByGID(body.GID)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, fmt.Sprintf("Error finding game %v: %v", body.GID, err.Error()))
		return
	}

	user, isAnon, err := handler.PlayTokenInteractor.Verify(body.Token, game.UID)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, err.Error())
		return
	}

	handler.util.renderer.Render(res, req, http.StatusOK, VerifyPlayTokenResponseV0{
		User:    user,
		IsAnon:  isAnon,
		Message: "Play token verified",
		Success: true,
	})
}

func (handler *Handler) CreatePlayToken(res http.ResponseWriter, req *http.Request) {
	userUID := req.Context().Value(UserUIDContextKey)
	if userUID == nil {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires authentication.")
		return
	}

	var body CreatePlayTokenRequestV0
	err := handler.util.DecodeRequestBody(req, &body)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnprocessableEntity, fmt.Sprintf("Request body parse error: %v", err.Error()))
		return
	}

	user, err := handler.UserInteractor.GetByUsername(body.Username)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, fmt.Sprintf("Could not find user: %v", err.Error()))
		return
	}

	if user.UID != userUID {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires authentication.")
		return
	}

	game, err := handler.GameInteractor.GetByGID(body.GID)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, fmt.Sprintf("Could not find game: %v", err.Error()))
		return
	}

	playToken, err := handler.PlayTokenInteractor.Create(user, game)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, err.Error())
		return
	}

	handler.util.renderer.Render(res, req, http.StatusOK, CreatePlayTokenResponseV0{
		PlayToken: playToken,
		Message:   "Play token created",
		Success:   true,
	})
}

func (handler *Handler) CreateAnonPlayToken(res http.ResponseWriter, req *http.Request) {
	var body CreateAnonPlayTokenRequestV0
	err := handler.util.DecodeRequestBody(req, &body)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnprocessableEntity, fmt.Sprintf("Request body parse error: %v", err.Error()))
		return
	}

	game, err := handler.GameInteractor.GetByGID(body.GID)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, fmt.Sprintf("Could not find game: %v", err.Error()))
		return
	}

	playToken, err := handler.PlayTokenInteractor.CreateAnon(game)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, err.Error())
		return
	}

	handler.util.renderer.Render(res, req, http.StatusOK, CreateAnonPlayTokenResponseV0{
		PlayToken: playToken,
		Message:   "Play token created",
		Success:   true,
	})
}
