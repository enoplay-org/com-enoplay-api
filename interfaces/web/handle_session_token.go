package web

import (
	"fmt"
	"net/http"

	"gitlab.com/enoplay/com-enoplay-api/domain"
)

type GetSessionTokenUserResponseV0 struct {
	User    domain.UserProfile `json:"user"`
	Message string             `json:"message,omitempty"`
	Success bool               `json:"success,omitempty"`
}

type VerifyAccessTokenResponseV0 struct {
	IsValid bool   `json:"isValid"`
	Message string `json:"message,omitempty"`
	Success bool   `json:"success,omitempty"`
}

type CreateRefreshTokenRequestV0 struct {
	UsernameOrEmail string `json:"usernameOrEmail"`
	Password        string `json:"password"`
}

type CreateRefreshTokenResponseV0 struct {
	RefreshToken string      `json:"refreshToken"`
	User         domain.User `json:"user"`
	Message      string      `json:"message,omitempty"`
	Success      bool        `json:"success,omitempty"`
}

type CreateAccessTokenResponseV0 struct {
	AccessToken string `json:"accessToken"`
	Message     string `json:"message,omitempty"`
	Success     bool   `json:"success,omitempty"`
}

type DeleteRefreshTokenResponseV0 struct {
	Message string `json:"message,omitempty"`
	Success bool   `json:"success,omitempty"`
}

type SessionTokenInteractor interface {
	VerifyAccess(token string) (string, error)
	VerifyRefresh(token string) (string, error)
	CreateAccess(refreshToken string) (string, error)
	CreateRefresh(usernameOrEmail, password string) (string, domain.User, error)
	DeleteRefresh(token string) error
}

func (handler *Handler) GetSessionTokenUser(res http.ResponseWriter, req *http.Request) {
	userUID := req.Context().Value(UserUIDContextKey)
	if userUID == nil {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires authentication")
		return
	}

	user, err := handler.UserInteractor.Get(userUID.(string))
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, err.Error())
		return
	}

	handler.util.renderer.Render(res, req, http.StatusOK, GetSessionTokenUserResponseV0{
		User:    domain.NewUserProfile(user),
		Message: "User profile retrieved",
		Success: true,
	})
}

func (handler *Handler) VerifyAccessToken(res http.ResponseWriter, req *http.Request) {
	accessToken := req.Context().Value(AccessTokenContextKey)
	if accessToken == "" {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires authorization")
		return
	}

	_, err := handler.SessionTokenInteractor.VerifyAccess(accessToken.(string))

	handler.util.renderer.Render(res, req, http.StatusOK, VerifyAccessTokenResponseV0{
		IsValid: err == nil,
		Message: "Access token verified",
		Success: true,
	})
}

func (handler *Handler) CreateAccessToken(res http.ResponseWriter, req *http.Request) {
	refreshToken := req.Context().Value(RefreshTokenContextKey)
	if refreshToken == "" {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires authentication")
		return
	}

	// Create new token
	accessToken, err := handler.SessionTokenInteractor.CreateAccess(refreshToken.(string))
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, fmt.Sprintf("Error creating access token: %v", err))
		return
	}

	handler.util.renderer.Render(res, req, http.StatusCreated, CreateAccessTokenResponseV0{
		AccessToken: accessToken,
		Message:     "Access token Created",
		Success:     true,
	})
}

func (handler *Handler) CreateRefreshToken(res http.ResponseWriter, req *http.Request) {
	oldRefreshToken := req.Context().Value(RefreshTokenContextKey)

	var body CreateRefreshTokenRequestV0
	err := handler.util.DecodeRequestBody(req, &body)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, fmt.Sprintf("Request body parse error: %v", err.Error()))
		return
	}

	// Revoke old token
	if oldRefreshToken != nil {
		handler.SessionTokenInteractor.DeleteRefresh(oldRefreshToken.(string))
	}

	// Create new token
	refreshToken, user, err := handler.SessionTokenInteractor.CreateRefresh(body.UsernameOrEmail, body.Password)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, fmt.Sprintf("Error creating refresh token: %v", err))
		return
	}

	handler.util.renderer.Render(res, req, http.StatusCreated, CreateRefreshTokenResponseV0{
		RefreshToken: refreshToken,
		User:         user,
		Message:      "Refresh token created",
		Success:      true,
	})
}

func (handler *Handler) DeleteRefreshToken(res http.ResponseWriter, req *http.Request) {
	refreshToken := req.Context().Value(RefreshTokenContextKey)
	if refreshToken == nil {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires authentication")
		return
	}

	// Revoke refresh token
	err := handler.SessionTokenInteractor.DeleteRefresh(refreshToken.(string))
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, err.Error())
		return
	}

	handler.util.renderer.Render(res, req, http.StatusCreated, DeleteRefreshTokenResponseV0{
		Message: "Refresh token deleted",
		Success: true,
	})
}
