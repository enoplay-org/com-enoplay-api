package repository

import (
	"gitlab.com/enoplay/com-enoplay-api/domain"
	mgo "gopkg.in/mgo.v2"
)

type Query map[string]interface{}
type Change mgo.Change
type Index mgo.Index

type DbHandler interface {
	Insert(name string, obj interface{}) error
	Update(name string, query Query, change Change, result interface{}) error
	UpdateAll(name string, query Query, change Query) (int, error)
	FindOne(name string, query Query, result interface{}) error
	FindAll(name string, query Query, result interface{}, limit int, sort string) error
	Aggregate(name string, queryList []Query, result interface{}) error
	Count(name string, query Query) (int, error)
	RemoveOne(name string, query Query) error
	RemoveAll(name string, query Query) error
	Exists(name string, query Query) bool
	DropCollection(name string) error
	DropDatabase() error
	EnsureIndex(name string, index mgo.Index) error

	ErrorTypeRecordNotFound() error
}

type DbRepo struct {
	dbHandlers map[string]DbHandler
	dbHandler  DbHandler
}

type MailHandler interface {
	Send(from, to, subject, content string) error
}

type PaymentHandler interface {
	Charge(source string, amount uint64, description string) (domain.PaymentReceipt, error)
}

type AppHandler interface {
	Delete(sessionToken string, aid string, withForce bool) error
}

type MediaHandler interface {
	Delete(sessionToken string, uid string) error
}
