package repository

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
	"time"

	"gitlab.com/enoplay/com-enoplay-api/domain"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const UserCollection = "users"
const UserDeletedCollection = "users_deleted"
const UserGameLibraryCollection = "users_game_library"
const UserGamePublishCollection = "users_game_publish"
const UserAppPublishCollection = "users_app_publish"
const UserGamePlayAccessCollection = "users_game_play_access"

type DbUserRepo struct {
	DbRepo
	mailHandler    MailHandler
	paymentHandler PaymentHandler
	mediaHandler   MediaHandler
	options        *DbUserRepoOptions
}

type DbUserRepoOptions struct {
	WWWHostURL    string
	MobileHostURL string
}

var emailConfirmationTemplate string
var passwordResetTemplate string
var userFeedbackTemplate string

func NewDbUserRepo(dbHandlers map[string]DbHandler, mailHandler MailHandler, paymentHandler PaymentHandler, mediaHandler MediaHandler, options *DbUserRepoOptions) *DbUserRepo {
	dbUserRepo := &DbUserRepo{}
	dbUserRepo.dbHandlers = dbHandlers
	dbUserRepo.dbHandler = dbHandlers["DbUserRepo"]
	dbUserRepo.mailHandler = mailHandler
	dbUserRepo.paymentHandler = paymentHandler
	dbUserRepo.mediaHandler = mediaHandler
	dbUserRepo.options = options

	// ensure that collection has the right text index
	// refactor building collection index
	err := dbUserRepo.dbHandler.EnsureIndex(UserCollection, mgo.Index{
		Key: []string{
			"$text:uid",
			"$text:username",
			"$text:alias",
		},
		Background: true,
		Sparse:     true,
	})
	if err != nil {
		fmt.Printf("Ensure User Index error: %v\n", err.Error())
	}

	// Email Confirmation Template
	emailConfirmationTemplateBytes, err := ioutil.ReadFile("interfaces/repository/email_templates/email_confirmation.html")
	if err != nil {
		log.Fatal(fmt.Errorf("Error loading email confirmation template: %v", err.Error()))
	}
	emailConfirmationTemplate = string(emailConfirmationTemplateBytes)

	// Password Reset Template
	passwordResetTemplateBytes, err := ioutil.ReadFile("interfaces/repository/email_templates/password_reset.html")
	if err != nil {
		log.Fatal(fmt.Errorf("Error loading password reset template: %v", err.Error()))
	}
	passwordResetTemplate = string(passwordResetTemplateBytes)

	// User Feedback Template
	userFeedbackTemplateBytes, err := ioutil.ReadFile("interfaces/repository/email_templates/user_feedback.html")
	if err != nil {
		log.Fatal(fmt.Errorf("Error loading user feedback template: %v", err.Error()))
	}
	userFeedbackTemplate = string(userFeedbackTemplateBytes)

	return dbUserRepo
}

func (repo *DbUserRepo) Get(uid string) (domain.User, error) {
	var user domain.User
	err := repo.dbHandler.FindOne(UserCollection, Query{"uid": uid}, &user)
	if err != nil {
		return domain.User{}, err
	}
	return user, nil
}

func (repo *DbUserRepo) GetByUsername(username string) (domain.User, error) {
	var user domain.User
	query := Query{
		"username": Query{
			"$regex":   fmt.Sprintf("^%v$", username),
			"$options": "mi",
		},
	}
	err := repo.dbHandler.FindOne(UserCollection, query, &user)
	return user, err
}

func (repo *DbUserRepo) GetAll() domain.Users {
	users := domain.Users{}
	err := repo.dbHandler.FindAll(UserCollection, nil, &users, 50, "")
	if err != nil {
		return domain.Users{}
	}
	return users
}

func (repo *DbUserRepo) Create(user domain.User) (domain.User, error) {
	user.UID = bson.NewObjectId().Hex()
	user.DateCreated = time.Now()

	// Insert User to UserCollection
	err := repo.dbHandler.Insert(UserCollection, user)
	if err != nil {
		return domain.User{}, err
	}

	// Setup User history objects
	repo.SetupUserGameLibrary(user.UID)
	repo.SetupUserGamePublishHistory(user.UID)
	repo.SetupUserAppPublishHistory(user.UID)
	repo.SetupUserGamePlayAccessHistory(user.UID)

	// Send email confirmation letter
	err = repo.SendEmailConfirmation(user)
	if err != nil {
		return domain.User{}, err
	}

	return user, nil
}

func (repo *DbUserRepo) UpdateAlias(user domain.User) (domain.User, error) {
	update := Query{
		"alias": user.Alias,
	}
	change := Change{
		Update:    Query{"$set": update},
		ReturnNew: true,
	}

	var updatedUser domain.User
	err := repo.dbHandler.Update(UserCollection, Query{"uid": user.UID}, change, &updatedUser)
	return updatedUser, err
}

func (repo *DbUserRepo) UpdateUsername(user domain.User) (domain.User, error) {
	update := Query{
		"username": user.Username,
	}
	change := Change{
		Update:    Query{"$set": update},
		ReturnNew: true,
	}

	var updatedUser domain.User
	err := repo.dbHandler.Update(UserCollection, Query{"uid": user.UID}, change, &updatedUser)
	return updatedUser, err
}

func (repo *DbUserRepo) UpdateEmail(user domain.User) (domain.User, error) {
	update := Query{
		"email":                 user.Email,
		"status":                domain.UserStatusPending, // Pending until email is confirmed
		"emailConfirmationCode": user.EmailConfirmationCode,
	}
	change := Change{
		Update:    Query{"$set": update},
		ReturnNew: true,
	}

	var updatedUser domain.User
	err := repo.dbHandler.Update(UserCollection, Query{"uid": user.UID}, change, &updatedUser)
	if err != nil {
		return domain.User{}, err
	}

	return updatedUser, repo.SendEmailConfirmation(user)
}

func (repo *DbUserRepo) UpdateDescription(user domain.User) (domain.User, error) {
	update := Query{
		"description": user.Description,
	}
	change := Change{
		Update:    Query{"$set": update},
		ReturnNew: true,
	}

	var updatedUser domain.User
	err := repo.dbHandler.Update(UserCollection, Query{"uid": user.UID}, change, &updatedUser)
	if err != nil {
		return domain.User{}, err
	}

	return updatedUser, nil
}

func (repo *DbUserRepo) UpdatePassword(user domain.User) error {
	update := Query{
		"hashedPassword":    user.HashedPassword,
		"passwordResetCode": "", // Reset Code
	}
	change := Change{
		Update: Query{"$set": update},
	}

	var updatedUser domain.User
	return repo.dbHandler.Update(UserCollection, Query{"uid": user.UID}, change, &updatedUser)
}

func (repo *DbUserRepo) UpdateEmailConfirmationCode(user domain.User) error {
	update := Query{
		"emailConfirmationCode": user.EmailConfirmationCode,
	}
	change := Change{
		Update: Query{"$set": update},
	}

	var updatedUser domain.User
	err := repo.dbHandler.Update(UserCollection, Query{"uid": user.UID}, change, &updatedUser)
	if err != nil {
		return fmt.Errorf("Error reseting email confirmation code in database: %v", err)
	}
	return repo.SendEmailConfirmation(user)
}

func (repo *DbUserRepo) ConfirmEmail(user domain.User) error {
	update := Query{
		"emailConfirmationCode": "",                      // Reset Code
		"status":                domain.UserStatusActive, // Activate User Account
	}
	change := Change{
		Update:    Query{"$set": update},
		ReturnNew: true,
	}

	var updatedUser domain.User
	err := repo.dbHandler.Update(UserCollection, Query{"uid": user.UID}, change, &updatedUser)
	return err
}

func (repo *DbUserRepo) ResetPassword(user domain.User) error {
	update := Query{
		"passwordResetCode": user.PasswordResetCode,
	}

	change := Change{
		Update: Query{"$set": update},
	}

	var updatedUser domain.User
	err := repo.dbHandler.Update(UserCollection, Query{"uid": user.UID}, change, &updatedUser)
	if err != nil {
		return err
	}

	err = repo.SendPasswordReset(user)
	if err != nil {
		return err
	}

	return nil
}

func (repo *DbUserRepo) ExistsByEmail(email string) bool {
	query := Query{
		"email": Query{
			"$regex":   fmt.Sprintf("^%v$", email),
			"$options": "mi",
		},
	}
	return repo.dbHandler.Exists(UserCollection, query)
}

func (repo *DbUserRepo) ExistsByUsername(username string) bool {
	query := Query{
		"username": Query{
			"$regex":   fmt.Sprintf("^%v$", username),
			"$options": "mi",
		},
	}
	return repo.dbHandler.Exists(UserCollection, query)
}

func (repo *DbUserRepo) FindByEmail(email string) (domain.User, error) {
	var user domain.User
	query := Query{
		"email": Query{
			"$regex":   fmt.Sprintf("^%v$", email),
			"$options": "mi",
		},
	}
	err := repo.dbHandler.FindOne(UserCollection, query, &user)
	return user, err
}

func (repo *DbUserRepo) FindByEmailConfirmationCode(code string) (domain.User, error) {
	var user domain.User
	err := repo.dbHandler.FindOne(UserCollection, Query{"emailConfirmationCode": code}, &user)
	return user, err
}

func (repo *DbUserRepo) FindByPasswordResetCode(code string) (domain.User, error) {
	var user domain.User
	err := repo.dbHandler.FindOne(UserCollection, Query{"passwordResetCode": code}, &user)
	return user, err
}

func (repo *DbUserRepo) Filter(field, query, lastID string, limit int, sort string) domain.Users {
	users := domain.Users{}

	// parse sort string
	allowedSortMap := map[string]bool{
		"_id":  true,
		"-_id": true,
	}
	// ensure that sort string is allowed
	// we are basically concerned about sorting on un-indexed keys
	if !allowedSortMap[sort] {
		sort = "-_id" // set it to default sort
	}

	q := Query{}
	if lastID != "" && bson.IsObjectIdHex(lastID) {
		if sort == "_id" {
			q["_id"] = Query{
				"$gt": bson.ObjectIdHex(lastID),
			}
		} else {
			q["_id"] = Query{
				"$lt": bson.ObjectIdHex(lastID),
			}
		}
	}

	if query != "" {
		if field != "" {
			q[field] = Query{
				"$regex":   fmt.Sprintf("^%v.*", query),
				"$options": "i",
			}
		} else {
			// if not field is specified, we do a text search on pre-defined text index
			q["$text"] = Query{
				"$search": query,
			}
		}
	}

	err := repo.dbHandler.FindAll(UserCollection, q, &users, limit, sort)
	if err != nil {
		fmt.Printf("FilterUsers Error: %v\n", err)
		return domain.Users{}
	}

	return users
}

const ConfirmEmailSender = "Enoplay <noreply@enoplay.com>"
const ConfirmEmailSubject = "Activate your Enoplay account"

// SendEmailConfirmation sends an email containing an email confirmation link
func (repo *DbUserRepo) SendEmailConfirmation(user domain.User) error {
	template := emailConfirmationTemplate
	template = strings.Replace(template, "{{ name }}", user.Alias, -1)
	template = strings.Replace(template, "{{ code }}", user.EmailConfirmationCode, -1)
	template = strings.Replace(template, "{{ wwwHostURL }}", repo.options.WWWHostURL, -1)

	return repo.mailHandler.Send(ConfirmEmailSender, user.Email, ConfirmEmailSubject, template)
}

const ResetPasswordSender = "Enoplay <noreply@enoplay.com>"
const ResetPasswordSubject = "Password reset request"

// SendPasswordReset sends an email containing a password reset link
func (repo *DbUserRepo) SendPasswordReset(user domain.User) error {
	template := passwordResetTemplate
	template = strings.Replace(template, "{{ name }}", user.Alias, -1)
	template = strings.Replace(template, "{{ code }}", user.PasswordResetCode, -1)
	template = strings.Replace(template, "{{ wwwHostURL }}", repo.options.WWWHostURL, -1)

	return repo.mailHandler.Send(ResetPasswordSender, user.Email, ResetPasswordSubject, template)
}

const SendFeedbackSender = "Enoplay <noreply@enoplay.com>"
const SendFeedbackRecipient = "feedback@enoplay.com"
const SendFeedbackSubject = "Enoplay User Feedback"

// SendFeedback sends an email with a feedback message
func (repo *DbUserRepo) SendFeedback(name, email, message string) error {
	template := userFeedbackTemplate
	template = strings.Replace(template, "{{ name }}", name, -1)
	template = strings.Replace(template, "{{ email }}", email, -1)
	template = strings.Replace(template, "{{ message }}", message, -1)

	return repo.mailHandler.Send(SendFeedbackSender, SendFeedbackRecipient, SendFeedbackSubject, template)
}

// Delete destroys a User
func (repo *DbUserRepo) Delete(sessionToken string, user domain.User) error {
	// Add User to deleted collection
	repo.dbHandler.Insert(UserDeletedCollection, user)

	// Delete User media
	repo.DeleteIcon(sessionToken, user)
	repo.DeleteBanner(sessionToken, user)

	// Remove User history
	repo.dbHandler.RemoveOne(UserGameLibraryCollection, Query{"userUID": user.UID})
	repo.dbHandler.RemoveOne(UserGamePlayAccessCollection, Query{"userUID": user.UID})
	repo.dbHandler.RemoveOne(UserGamePublishCollection, Query{"userUID": user.UID})
	repo.dbHandler.RemoveOne(UserAppPublishCollection, Query{"userUID": user.UID})

	// Remove User from collection
	return repo.dbHandler.RemoveOne(UserCollection, Query{"uid": user.UID})
}

// DeleteIcon destroys the icon image of a User
func (repo *DbUserRepo) DeleteIcon(sessionToken string, user domain.User) (domain.User, error) {
	// Delete User Icon
	err := repo.mediaHandler.Delete(sessionToken, user.Media.Icon.UID)
	if err != nil {
		return domain.User{}, fmt.Errorf("Error deleting user icon: %v", err)
	}

	user.Media.Icon.UID = ""
	user.Media.Icon.Source = ""

	update := Query{
		"media.icon": user.Media.Icon,
	}
	change := Change{
		Update:    Query{"$set": update},
		ReturnNew: true,
	}

	var updatedUser domain.User
	err = repo.dbHandler.Update(UserCollection, Query{"uid": user.UID}, change, &updatedUser)
	return updatedUser, err
}

// DeleteBanner destroys the banner image of a User
func (repo *DbUserRepo) DeleteBanner(sessionToken string, user domain.User) (domain.User, error) {
	// Delete User Banner
	err := repo.mediaHandler.Delete(sessionToken, user.Media.Banner.UID)
	if err != nil {
		return domain.User{}, fmt.Errorf("Error deleting user banner: %v", err)
	}

	user.Media.Banner.UID = ""
	user.Media.Banner.Source = ""

	update := Query{
		"media.banner": user.Media.Banner,
	}
	change := Change{
		Update:    Query{"$set": update},
		ReturnNew: true,
	}

	var updatedUser domain.User
	err = repo.dbHandler.Update(UserCollection, Query{"uid": user.UID}, change, &updatedUser)
	return updatedUser, err
}

// GetGamePublishHistory retrieves a Users history of published Games
func (repo *DbUserRepo) GetGamePublishHistory(userUID string) (domain.UserGamePublishHistory, error) {
	var gamePublishHistory domain.UserGamePublishHistory
	err := repo.dbHandler.FindOne(UserGamePublishCollection, Query{"userUID": userUID}, &gamePublishHistory)
	if err != nil {
		if err.Error() != repo.dbHandler.ErrorTypeRecordNotFound().Error() {
			return domain.UserGamePublishHistory{}, err
		}

		gamePublishHistory = repo.SetupUserGamePublishHistory(userUID)
	}
	return gamePublishHistory, nil
}

// SetupUserGameLibrary initializes a list of Games a User is collecting
func (repo *DbUserRepo) SetupUserGameLibrary(userUID string) domain.UserGameLibrary {
	gameLibrary := domain.UserGameLibrary{
		UserUID: userUID,
		Library: make(map[string]domain.UserGameLibraryItem),
	}
	repo.dbHandler.Insert(UserGameLibraryCollection, gameLibrary)
	return gameLibrary
}

// SetupUserGamePublishHistory initializes a history of published Games for a given User
func (repo *DbUserRepo) SetupUserGamePublishHistory(userUID string) domain.UserGamePublishHistory {
	gamePublishHistory := domain.UserGamePublishHistory{
		UserUID: userUID,
		History: make(map[string]domain.UserGamePublishHistoryItem),
	}
	repo.dbHandler.Insert(UserGamePublishCollection, gamePublishHistory)
	return gamePublishHistory
}

// SetupUserAppPublishHistory initializes a history of published Apps for a given User
func (repo *DbUserRepo) SetupUserAppPublishHistory(userUID string) domain.UserAppPublishHistory {
	appPublishHistory := domain.UserAppPublishHistory{
		UserUID: userUID,
		History: make(map[string]domain.UserAppPublishHistoryItem),
	}
	repo.dbHandler.Insert(UserAppPublishCollection, appPublishHistory)
	return appPublishHistory
}

// SetupUserGamePlayAccessHistory initializes a history of Games a User has access to play
func (repo *DbUserRepo) SetupUserGamePlayAccessHistory(userUID string) domain.UserGamePlayAccessHistory {
	playAccessHistory := domain.UserGamePlayAccessHistory{
		UserUID: userUID,
		History: make(map[string]domain.UserGamePlayAccessHistoryItem),
	}
	repo.dbHandler.Insert(UserGamePlayAccessCollection, playAccessHistory)
	return playAccessHistory
}

// PublishGame adds a Game to a Users publish history
func (repo *DbUserRepo) PublishGame(userUID string, gamePublishItem domain.UserGamePublishHistoryItem) error {
	gamePublishItem.DateAdded = time.Now()

	// Update User's published Games
	queryMap := make(map[string]interface{})
	queryMap[fmt.Sprintf("history.%v", gamePublishItem.GameUID)] = gamePublishItem

	update := Query(queryMap)
	change := Change{
		Update: Query{"$set": update},
	}

	var updatedHistory domain.UserGamePublishHistory
	return repo.dbHandler.Update(UserGamePublishCollection, Query{"userUID": userUID}, change, &updatedHistory)
}

// UnpublishGame removes a Game from a Users publish history
func (repo *DbUserRepo) UnpublishGame(userUID string, gameUID string) error {
	queryMap := make(map[string]interface{})
	queryMap[fmt.Sprintf("history.%v", gameUID)] = 1

	update := Query(queryMap)
	change := Change{
		Update: Query{"$unset": update},
	}

	var updatedHistory domain.UserGamePublishHistory
	return repo.dbHandler.Update(UserGamePublishCollection, Query{"userUID": userUID}, change, &updatedHistory)
}

// PurchaseGame purchases a Game for a given User
func (repo *DbUserRepo) PurchaseGame(user domain.User, game domain.Game, sourceToken string) (domain.UserGamePlayAccessHistoryItem, error) {
	receipt, err := repo.paymentHandler.Charge(sourceToken, game.Price.Amount, fmt.Sprintf("User %v purchasing Game %v", user.UID, game.UID))
	if err != nil {
		return domain.UserGamePlayAccessHistoryItem{}, fmt.Errorf("Error purchasing game from repo: %v", err.Error())
	}

	gamePlayAccessHistoryItem := domain.UserGamePlayAccessHistoryItem{
		GameUID:   game.UID,
		Type:      domain.GamePlayAccessTypePurchase,
		DateAdded: time.Now(),
		Receipt:   receipt,
	}

	// Setup record
	queryMap := make(map[string]interface{})
	queryMap[fmt.Sprintf("history.%v", game.UID)] = gamePlayAccessHistoryItem

	update := Query(queryMap)
	change := Change{
		Update: Query{"$set": update},
	}

	// Update record in database
	var gamePlayAccessHistory domain.UserGamePlayAccessHistory
	err = repo.dbHandler.Update(UserGamePlayAccessCollection, Query{"userUID": user.UID}, change, &gamePlayAccessHistory)
	if err != nil {
		if err.Error() != repo.dbHandler.ErrorTypeRecordNotFound().Error() {
			return domain.UserGamePlayAccessHistoryItem{}, err
		}

		// Try again
		gamePlayAccessHistory = repo.SetupUserGamePlayAccessHistory(user.UID)
		err = repo.dbHandler.Update(UserGamePlayAccessCollection, Query{"userUID": user.UID}, change, &gamePlayAccessHistory)
	}

	return gamePlayAccessHistoryItem, err
}

// HasPlayAccess checks if a User has access to play a Game
func (repo *DbUserRepo) HasPlayAccess(userUID string, gameUID string) bool {
	var userGamePlayAccessHistory domain.UserGamePlayAccessHistory
	err := repo.dbHandler.FindOne(UserGamePlayAccessCollection, Query{"userUID": userUID}, &userGamePlayAccessHistory)
	if err != nil {
		return false
	}

	return userGamePlayAccessHistory.History[gameUID] != domain.UserGamePlayAccessHistoryItem{}
}
