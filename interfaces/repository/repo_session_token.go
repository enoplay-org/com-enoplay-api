package repository

import (
	"crypto/rsa"
	"fmt"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/enoplay/com-enoplay-api/domain"
	"gopkg.in/mgo.v2/bson"
)

const RefreshSessionTokenCollection = "tokens_refresh_revoked"

var rsaSampleSecret = []byte("something-super-secret")

type DbSessionTokenRepo struct {
	DbRepo
	TAOptions *TokenAuthorityOptions
}

type TokenAuthorityOptions struct {
	PrivateSigningKey *rsa.PrivateKey
	PublicSigningKey  *rsa.PublicKey
}

func NewDbSessionTokenRepo(dbHandlers map[string]DbHandler, taOptions *TokenAuthorityOptions) *DbSessionTokenRepo {
	dbSessionTokenRepo := &DbSessionTokenRepo{}
	dbSessionTokenRepo.dbHandlers = dbHandlers
	dbSessionTokenRepo.dbHandler = dbHandlers["DbSessionTokenRepo"]
	dbSessionTokenRepo.TAOptions = taOptions
	return dbSessionTokenRepo
}

func NewTokenAuthorityOptions(privateKey *rsa.PrivateKey, publicKey *rsa.PublicKey) *TokenAuthorityOptions {
	taOptions := &TokenAuthorityOptions{
		PrivateSigningKey: privateKey,
		PublicSigningKey:  publicKey,
	}

	return taOptions
}

// VerifyAccess verifies an access session token
func (repo *DbSessionTokenRepo) VerifyAccess(tokenString string) (domain.SessionTokenClaim, error) {
	// Ensure token is valid
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return repo.TAOptions.PublicSigningKey, nil
	})
	if err != nil {
		return domain.SessionTokenClaim{}, fmt.Errorf("Error parsing access token: %v", err)
	}

	var claim domain.SessionTokenClaim

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		claim.UserUID = claims["userUID"].(string)
		claim.Type = claims["type"].(string)
		claim.DateExpired = time.Unix(int64(claims["dateExpired"].(float64)), 0)
		claim.DateIssued = time.Unix(int64(claims["dateIssued"].(float64)), 0)
		claim.JTI = claims["jti"].(string)
	} else {
		return domain.SessionTokenClaim{}, fmt.Errorf("Invalid access token")
	}

	// Ensure token is access token
	if claim.Type != domain.SessionTokenTypeAccess {
		return domain.SessionTokenClaim{}, fmt.Errorf("Invalid access token")
	}

	// Ensure token has not expired
	if time.Now().Sub(claim.DateExpired).Seconds() > 0 {
		return domain.SessionTokenClaim{}, fmt.Errorf("Access token has expired")
	}

	return claim, nil
}

// VerifyRefresh verifies a refresh token
func (repo *DbSessionTokenRepo) VerifyRefresh(tokenString string) (domain.SessionTokenClaim, error) {
	// Ensure token is valid
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return repo.TAOptions.PublicSigningKey, nil
	})
	if err != nil {
		return domain.SessionTokenClaim{}, fmt.Errorf("Error parsing refresh token: %v", err)
	}

	var claim domain.SessionTokenClaim

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		claim.UserUID = claims["userUID"].(string)
		claim.Type = claims["type"].(string)
		claim.DateExpired = time.Unix(int64(claims["dateExpired"].(float64)), 0)
		claim.DateIssued = time.Unix(int64(claims["dateIssued"].(float64)), 0)
		claim.JTI = claims["jti"].(string)
	} else {
		return domain.SessionTokenClaim{}, fmt.Errorf("Invalid refresh token")
	}

	// Ensure token is refresh token
	if claim.Type != domain.SessionTokenTypeRefresh {
		return domain.SessionTokenClaim{}, fmt.Errorf("Invalid refresh token")
	}

	// Ensure token has not expired
	if time.Now().Sub(claim.DateExpired).Seconds() > 0 {
		return domain.SessionTokenClaim{}, fmt.Errorf("Token has expired")
	}

	// Ensure token hasn't been revoked
	isRevoked := repo.dbHandler.Exists(RefreshSessionTokenCollection, Query{"_id": bson.ObjectIdHex(claim.JTI)})
	if isRevoked {
		return domain.SessionTokenClaim{}, fmt.Errorf("Invalid refresh token")
	}

	return claim, nil
}

func (repo *DbSessionTokenRepo) Create(claim domain.SessionTokenClaim) (string, error) {
	claims := make(jwt.MapClaims)
	claims["userUID"] = claim.UserUID
	claims["type"] = claim.Type
	claims["dateExpired"] = claim.DateExpired.Unix()
	claims["dateIssued"] = time.Now().Unix()
	claims["jti"] = generateJTI()

	token := jwt.NewWithClaims(jwt.SigningMethodRS512, claims)

	return token.SignedString(repo.TAOptions.PrivateSigningKey)
}

func (repo *DbSessionTokenRepo) DeleteRefresh(revoked domain.RevokedSessionToken) error {
	revoked.DateRevoked = time.Now()
	err := repo.dbHandler.Insert(RefreshSessionTokenCollection, revoked)
	if err != nil {
		err = fmt.Errorf("Error deleting session token")
	}
	return err
}

func generateJTI() string {
	// We will use mongodb's object id as JTI
	// we then will use this id to blacklist tokens,
	// along with `exp` and `iat` claims.
	// As far as collisions go, ObjectId is guaranteed unique
	// within a collection; and this case our collection is `sessions`
	return bson.NewObjectId().Hex()
}
