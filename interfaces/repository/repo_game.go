package repository

import (
	"fmt"
	"time"

	"gitlab.com/enoplay/com-enoplay-api/domain"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const GameCollection = "games"
const GameDeletedCollection = "games_deleted"
const GameUserPurchaseCollection = "games_user_purchase"
const GameUserBanCollection = "games_user_ban"

type DbGameRepo struct {
	DbRepo
	appHandler   AppHandler
	mediaHandler MediaHandler
}

func NewDbGameRepo(dbHandlers map[string]DbHandler, appHandler AppHandler, mediaHandler MediaHandler) *DbGameRepo {
	dbGameRepo := &DbGameRepo{}
	dbGameRepo.dbHandlers = dbHandlers
	dbGameRepo.dbHandler = dbHandlers["DbGameRepo"]
	dbGameRepo.appHandler = appHandler
	dbGameRepo.mediaHandler = mediaHandler

	// ensure that collection has the right text index
	// refactor building collection index
	err := dbGameRepo.dbHandler.EnsureIndex(GameCollection, mgo.Index{
		Key: []string{
			"$text:uid",
			"$text:gid",
			"$text:title",
			"$text:description",
			"$text:tags",
		},
		Background: true,
		Sparse:     true,
	})
	if err != nil {
		fmt.Printf("Ensure Games Index: %v\n", err.Error())
	}

	return dbGameRepo
}

// Get retrieves a Game by its uid
func (repo *DbGameRepo) Get(uid string) (domain.Game, error) {
	var game domain.Game
	err := repo.dbHandler.FindOne(GameCollection, Query{"uid": uid}, &game)
	if err != nil {
		return domain.Game{}, fmt.Errorf("Error finding game by uid: %v", err)
	}
	return game, nil
}

// GetByGID retrieves a Game by its gid
func (repo *DbGameRepo) GetByGID(gid string) (domain.Game, error) {
	var game domain.Game
	err := repo.dbHandler.FindOne(GameCollection, Query{"gid": gid}, &game)
	if err != nil {
		return domain.Game{}, fmt.Errorf("Error finding game by gid: %v", err)
	}
	return game, nil
}

// GetAll retrieves all Games
func (repo *DbGameRepo) GetAll() domain.Games {
	games := domain.Games{}
	err := repo.dbHandler.FindAll(GameCollection, nil, &games, 50, "")
	if err != nil {
		return domain.Games{}
	}
	return games
}

// Filter searches for a list of Games
func (repo *DbGameRepo) Filter(queryList map[string]interface{}, limit int, sort string) domain.Games {
	games := domain.Games{}

	// parse sort string
	allowedSortMap := map[string]bool{
		"_id":  true,
		"-_id": true,
	}
	// ensure that sort string is allowed
	// we are basically concerned about sorting on un-indexed keys
	if !allowedSortMap[sort] {
		sort = "-_id" // set it to default sort
	}

	aggregateList := []Query{}

	// Allow searching by a field
	field := queryList["field"].(string)
	fieldQuery := queryList["fieldQuery"].(string)

	if fieldQuery != "" {
		query := Query{}

		// Search by a specific field
		if field != "" {
			subQuery := Query{}
			subQuery[field] = Query{
				"$regex":   fmt.Sprintf("^%v.*", fieldQuery),
				"$options": "i",
			}
			query["$match"] = subQuery
		} else {
			// if no field is specified, we do a text search on pre-defined text index
			query["$match"] = Query{
				"$text": Query{
					"$search": fieldQuery,
				},
			}
		}

		aggregateList = append(aggregateList, query)
	}

	// Search for public games
	if !(queryList["field"] == "uid" || queryList["field"] == "gid") {
		query := Query{
			"$match": Query{
				"privacy": domain.GamePrivacyPublic,
			},
		}

		aggregateList = append(aggregateList, query)
	}

	// Add browser support requirements
	if queryList["browser"] != "" {
		browserDesktopKey := fmt.Sprintf("systemSupport.browser.%v.desktop", queryList["browser"])
		browserMobileKey := fmt.Sprintf("systemSupport.browser.%v.mobile", queryList["browser"])

		browserDesktopQuery := Query{}
		browserDesktopQuery[browserDesktopKey] = true

		browserMobileQuery := Query{}
		browserMobileQuery[browserMobileKey] = true

		conditionList := []Query{
			browserDesktopQuery,
			browserMobileQuery,
		}

		// Either desktop or mobile must be supported
		query := Query{
			"$match": Query{
				"$or": conditionList,
			},
		}

		aggregateList = append(aggregateList, query)
	}

	// Add device support requirements
	if queryList["device"] != "" {
		deviceKey := queryList["device"].(string)

		browserKeyChrome := fmt.Sprintf("systemSupport.browser.%v.%v", domain.GameBrowserChrome, deviceKey)
		browserKeyFirefox := fmt.Sprintf("systemSupport.browser.%v.%v", domain.GameBrowserFirefox, deviceKey)
		browserKeySafari := fmt.Sprintf("systemSupport.browser.%v.%v", domain.GameBrowserSafari, deviceKey)

		deviceConditionChrome := Query{}
		deviceConditionChrome[browserKeyChrome] = true

		deviceConditionFirefox := Query{}
		deviceConditionFirefox[browserKeyFirefox] = true

		deviceConditionSafari := Query{}
		deviceConditionSafari[browserKeySafari] = true

		// Note: Edge and IE don't have mobile browsers afaik

		conditionList := []Query{
			deviceConditionChrome,
			deviceConditionFirefox,
			deviceConditionSafari,
		}

		// Must have device support on at least 1 browser
		query := Query{
			"$match": Query{
				"$or": conditionList,
			},
		}

		aggregateList = append(aggregateList, query)
	}

	err := repo.dbHandler.Aggregate(GameCollection, aggregateList, &games)
	if err != nil {
		return domain.Games{}
	}
	return games
}

// Create generates a new Game
func (repo *DbGameRepo) Create(game domain.Game) (domain.Game, error) {
	game.UID = bson.NewObjectId().Hex()
	game.DateCreated = time.Now()
	game.DateLastModified = time.Now()

	// Insert Game to GameCollection
	err := repo.dbHandler.Insert(GameCollection, game)
	if err != nil {
		return domain.Game{}, fmt.Errorf("Error creating game from repository: %v", err)
	}

	// Setup Game history objects
	repo.SetupGameUserPurchaseHistory(game.UID)
	repo.SetupGameUserBanHistory(game.UID)

	return game, nil
}

// UpdateTitle updates the title of a Game
func (repo *DbGameRepo) UpdateTitle(game domain.Game) (domain.Game, error) {
	update := Query{
		"title": game.Title,
	}
	change := Change{
		Update:    Query{"$set": update},
		ReturnNew: true,
	}

	var updatedGame domain.Game
	err := repo.dbHandler.Update(GameCollection, Query{"uid": game.UID}, change, &updatedGame)
	return updatedGame, err
}

// UpdateDescription updates the description of a Game
func (repo *DbGameRepo) UpdateDescription(game domain.Game) (domain.Game, error) {
	update := Query{
		"description": game.Description,
	}
	change := Change{
		Update:    Query{"$set": update},
		ReturnNew: true,
	}

	var updatedGame domain.Game
	err := repo.dbHandler.Update(GameCollection, Query{"uid": game.UID}, change, &updatedGame)
	return updatedGame, err
}

// UpdatePrice updates the price of a Game
func (repo *DbGameRepo) UpdatePrice(game domain.Game) (domain.Game, error) {
	update := Query{
		"price": game.Price,
	}
	change := Change{
		Update:    Query{"$set": update},
		ReturnNew: true,
	}

	var updatedGame domain.Game
	err := repo.dbHandler.Update(GameCollection, Query{"uid": game.UID}, change, &updatedGame)
	return updatedGame, err
}

// UpdatePrivacy updates the privacy of a Game
func (repo *DbGameRepo) UpdatePrivacy(game domain.Game) (domain.Game, error) {
	update := Query{
		"privacy": game.Privacy,
	}
	change := Change{
		Update:    Query{"$set": update},
		ReturnNew: true,
	}

	var updatedGame domain.Game
	err := repo.dbHandler.Update(GameCollection, Query{"uid": game.UID}, change, &updatedGame)
	return updatedGame, err
}

// Delete destroys a Game
func (repo *DbGameRepo) Delete(sessionToken string, game domain.Game) error {
	// Add Game to deleted collection
	repo.dbHandler.Insert(GameDeletedCollection, game)

	// Delete Game media
	repo.DeleteThumbnail(sessionToken, game)
	repo.DeleteImages(sessionToken, game)

	// Delete Game App
	err := repo.DeleteApp(sessionToken, game)
	if err != nil {
		return fmt.Errorf("Error deleteting Game App: %v", err)
	}

	// Remove Game history
	repo.dbHandler.RemoveOne(GameUserPurchaseCollection, Query{"gameUID": game.UID})
	repo.dbHandler.RemoveOne(GameUserBanCollection, Query{"gameUID": game.UID})

	// Remove Game from collection
	return repo.dbHandler.RemoveOne(GameCollection, Query{"uid": game.UID})
}

// DeleteThumbnail destroys the thumbnail image of a Game
func (repo *DbGameRepo) DeleteThumbnail(sessionToken string, game domain.Game) (domain.Game, error) {
	// Delete Game Thumbnail
	err := repo.mediaHandler.Delete(sessionToken, game.Media.Thumbnail.UID)
	if err != nil {
		return domain.Game{}, fmt.Errorf("Error deleting game thumbnail media: %v", err)
	}

	game.Media.Thumbnail.UID = ""
	game.Media.Thumbnail.Source = ""

	update := Query{
		"media.thumbnail": game.Media.Thumbnail,
	}
	change := Change{
		Update:    Query{"$set": update},
		ReturnNew: true,
	}

	var updatedGame domain.Game
	err = repo.dbHandler.Update(GameCollection, Query{"uid": game.UID}, change, &updatedGame)
	return updatedGame, err
}

// DeleteImages destroys the media images of a Game
func (repo *DbGameRepo) DeleteImages(sessionToken string, game domain.Game) (domain.Game, error) {
	// Delete Media Images
	for imageUID := range game.Media.Images {
		repo.mediaHandler.Delete(sessionToken, imageUID)
		delete(game.Media.Images, imageUID)
	}

	// Setup record to update
	update := Query{
		"media.images": game.Media.Images,
	}
	change := Change{
		Update:    Query{"$set": update},
		ReturnNew: true,
	}

	// Update Game record
	var updatedGame domain.Game
	err := repo.dbHandler.Update(GameCollection, Query{"uid": game.UID}, change, &updatedGame)
	return updatedGame, err
}

// ExistsByGID checks if a Game exists by a specified gid
func (repo *DbGameRepo) ExistsByGID(gid string) bool {
	return repo.dbHandler.Exists(GameCollection, Query{"gid": gid})
}

// UpdateSystemBrowserSupport updates the supported browsers of a Game
func (repo *DbGameRepo) UpdateSystemBrowserSupport(game domain.Game) (domain.Game, error) {
	// Setup record to update
	update := Query{
		"systemSupport.browser": game.SystemSupport.Browser,
	}
	change := Change{
		Update:    Query{"$set": update},
		ReturnNew: true,
	}

	// Update Game record
	var updatedGame domain.Game
	err := repo.dbHandler.Update(GameCollection, Query{"uid": game.UID}, change, &updatedGame)
	return updatedGame, err
}

// IsUserBanned checks if a User is banned from a Game
func (repo *DbGameRepo) IsUserBanned(gameUID string, userUID string) bool {

	var gameUserBanHistory domain.GameUserBanHistory
	err := repo.dbHandler.FindOne(GameUserBanCollection, Query{"gameUID": gameUID}, &gameUserBanHistory)
	if err != nil {
		return false
	}

	exists := gameUserBanHistory.History[userUID] != domain.GameUserBanHistoryItem{}
	return exists
}

// UpdateUserPurchaseHistory adds a purchase record for a Game
func (repo *DbGameRepo) UpdateUserPurchaseHistory(gameUID string, purchase domain.GameUserPurchaseHistoryItem) error {

	// Setup record
	var queryMap = make(map[string]interface{})
	queryMap[fmt.Sprintf("history.%v", purchase.UserUID)] = purchase

	update := Query(queryMap)
	change := Change{
		Update:    Query{"$set": update},
		ReturnNew: true,
	}

	// Update record in database
	var userPurchaseHistory domain.GameUserPurchaseHistory
	err := repo.dbHandler.Update(GameUserPurchaseCollection, Query{"gameUID": gameUID}, change, &userPurchaseHistory)
	if err != nil {
		if err.Error() != repo.dbHandler.ErrorTypeRecordNotFound().Error() {
			return err
		}

		// Try again
		userPurchaseHistory = repo.SetupGameUserPurchaseHistory(gameUID)
		err = repo.dbHandler.Update(GameUserPurchaseCollection, Query{"gameUID": gameUID}, change, &userPurchaseHistory)
	}

	return err
}

// SetupGameUserPurchaseHistory initializes a history of Users who have purchased a Game
func (repo *DbGameRepo) SetupGameUserPurchaseHistory(gameUID string) domain.GameUserPurchaseHistory {
	userPurchaseHistory := domain.GameUserPurchaseHistory{
		GameUID: gameUID,
		History: make(map[string]domain.GameUserPurchaseHistoryItem),
	}
	repo.dbHandler.Insert(GameUserPurchaseCollection, userPurchaseHistory)
	return userPurchaseHistory
}

// SetupGameUserBanHistory initializes a history of Users who are banned from a Game
func (repo *DbGameRepo) SetupGameUserBanHistory(gameUID string) domain.GameUserBanHistory {
	userBanHistory := domain.GameUserBanHistory{
		GameUID: gameUID,
		History: make(map[string]domain.GameUserBanHistoryItem),
	}
	repo.dbHandler.Insert(GameUserBanCollection, userBanHistory)
	return userBanHistory
}

// DeleteApp deletes the Game App
func (repo *DbGameRepo) DeleteApp(sessionToken string, game domain.Game) error {
	return repo.appHandler.Delete(sessionToken, game.App.AID, true)
}
