# 🌎 [api.enoplay.com](https://api.enoplay.com)

> API for managing games, users and sessions for Enoplay.

## 📘 [Documentation](https://gitlab.com/enoplay-org/com-enoplay-api/tree/master/doc)

## 🔨 Build Setup

``` bash
# install dependencies
go get ./...

# update Godeps dependencies
godep save ./...

# serve at localhost:8089
go run main.go

# run unit tests
go test
```
