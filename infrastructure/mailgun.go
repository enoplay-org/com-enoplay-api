package infrastructure

import mailgun "gopkg.in/mailgun/mailgun-go.v1"

type MailgunHandler struct {
	mg      mailgun.Mailgun
	options *MailgunOptions
}

type MailgunOptions struct {
	Domain        string
	PrivateApiKey string
	PublicApiKey  string
}

func NewMailgunHandler(options MailgunOptions) *MailgunHandler {
	mailHandler := &MailgunHandler{
		options: &options,
	}
	mg := mailgun.NewMailgun(options.Domain, options.PrivateApiKey, options.PublicApiKey)
	mailHandler.mg = mg

	return mailHandler
}

func (mailer *MailgunHandler) Send(from, to, subject, content string) error {
	message := mailer.mg.NewMessage(from, subject, "", to)
	message.SetHtml(content)
	_, _, err := mailer.mg.Send(message)
	return err
}
