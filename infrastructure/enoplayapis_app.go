package infrastructure

import (
	"bytes"
	"fmt"
	"net/http"
)

type EnoplayAppOptions struct {
	PrivateApiKey string
	ApiBase       string
}

type EnoplayAppHandler struct {
	options *EnoplayAppOptions
}

func NewEnoplayAppHandler(options EnoplayAppOptions) *EnoplayAppHandler {
	handler := &EnoplayAppHandler{
		options: &options,
	}
	return handler
}

func (handler *EnoplayAppHandler) Delete(sessionToken string, aid string, withForce bool) error {
	var jsonBody = []byte(fmt.Sprintf(`{"force": %v}`, withForce))
	req, err := http.NewRequest("DELETE", fmt.Sprintf("%v/apps/%v", handler.options.ApiBase, aid), bytes.NewBuffer(jsonBody))
	if err != nil {
		return fmt.Errorf("Error creating delete request from Enoplay App: %v", err)
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %v", sessionToken))
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("Error creating delete response from Enoplay App: %v", err)
	}
	defer resp.Body.Close()
	return nil
}
