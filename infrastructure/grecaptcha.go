package infrastructure

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

// Google recaptcha service
const GoogleRecpatchaService = "Google"

type GrecaptchaHandler struct {
	options *GrecaptchaOptions
}

type GrecaptchaOptions struct {
	SecretKey string
}

type VerifyResponse struct {
	Success    bool          `json:"success"`
	HostName   string        `json:"hostname"`
	ErrorCodes []interface{} `json:"error-codes"`
}

func NewGrecaptchaHandler(options *GrecaptchaOptions) *GrecaptchaHandler {
	grecaptchaHandler := &GrecaptchaHandler{
		options: options,
	}

	return grecaptchaHandler
}

func (gre *GrecaptchaHandler) Verify(recaptcha string) bool {
	// https://developers.google.com/recaptcha/docs/verify

	if recaptcha == "" {
		return false
	}

	// Setup request
	var jsonBody = []byte(fmt.Sprintf(`{}`))
	url := fmt.Sprintf("https://www.google.com/recaptcha/api/siteverify?secret=%v&response=%v", gre.options.SecretKey, recaptcha)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonBody))
	if err != nil {
		return false
	}
	req.Header.Set("Content-Type", "application/json")

	// Send request
	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return false
	}
	defer res.Body.Close()

	// Check response
	var responseBody VerifyResponse
	err = DecodeResponseBody(res, &responseBody)
	if err != nil {
		return false
	}

	// Respond with success status
	return responseBody.Success
}

func DecodeResponseBody(res *http.Response, target interface{}) error {
	decoder := json.NewDecoder(res.Body)
	return decoder.Decode(target)
}
