package infrastructure

import (
	"fmt"

	"gitlab.com/enoplay/com-enoplay-api/domain"

	stripe "github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/charge"
	"github.com/stripe/stripe-go/client"
)

const StripeService = "stripe"

type StripeOptions struct {
	SecretApiKey string
}

type StripeHandler struct {
	st      *client.API
	options *StripeOptions
}

func NewStripeHandler(options StripeOptions) *StripeHandler {
	handler := &StripeHandler{
		options: &options,
	}
	stripe.Key = options.SecretApiKey
	return handler
}

func (handler *StripeHandler) Charge(source string, amount uint64, description string) (domain.PaymentReceipt, error) {
	chargeParams := &stripe.ChargeParams{
		Amount:   amount,
		Currency: "usd",
		Desc:     description,
	}

	chargeParams.SetSource(source)
	ch, err := charge.New(chargeParams)
	if err != nil {
		return domain.PaymentReceipt{}, fmt.Errorf("Error charging from %v: %v", StripeService, err)
	}

	receipt := domain.PaymentReceipt{
		ID:      ch.ID,
		Name:    ch.ReceiptNumber,
		Service: StripeService,
		Amount:  ch.Amount,
	}

	return receipt, nil
}
