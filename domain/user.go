package domain

import "time"

type UserRepository interface {
	Get(uid string) (User, error)
	GetByUsername(username string) (User, error)
	GetAll() Users

	Create(user User) (User, error)

	UpdateAlias(user User) (User, error)
	UpdateUsername(user User) (User, error)
	UpdateEmail(user User) (User, error)
	UpdateDescription(user User) (User, error)
	UpdatePassword(user User) error
	UpdateEmailConfirmationCode(user User) error

	Delete(sessionToken string, user User) error

	ConfirmEmail(user User) error
	ResetPassword(user User) error
	FindByEmail(email string) (User, error)
	FindByEmailConfirmationCode(code string) (User, error)
	FindByPasswordResetCode(code string) (User, error)

	ExistsByEmail(email string) bool
	ExistsByUsername(username string) bool
	Filter(field, query, lastID string, limit int, sort string) Users

	SendFeedback(name, email, message string) error

	GetGamePublishHistory(userUID string) (UserGamePublishHistory, error)
	PublishGame(userUID string, gamePublishItem UserGamePublishHistoryItem) error
	UnpublishGame(userUID string, gameUID string) error

	PurchaseGame(user User, game Game, sourceToken string) (UserGamePlayAccessHistoryItem, error)
	HasPlayAccess(userUID string, gameUID string) bool
}

type User struct {
	UID      string `json:"uid" bson:"uid,omitempty"`
	Username string `json:"username" bson:"username"`
	Alias    string `json:"alias" bson:"alias"`

	Description string    `json:"description" bson:"description"`
	Media       UserMedia `json:"media" bson:"media"`

	DateCreated time.Time `json:"dateCreated,omitempty" bson:"dateCreated"`
	LastOnline  time.Time `json:"lastOnline,omitempty" bson:"lastOnline"`

	// fields not exposed to JSON
	// Settings              map[string]int `json:"-" bson:"settings"`
	Status                string `json:"-" bson:"status"`
	Email                 string `json:"-" bson:"email"`
	EmailConfirmationCode string `json:"-" bson:"emailConfirmationCode"`
	PasswordResetCode     string `json:"-" bson:"passwordResetCode"`
	HashedPassword        string `json:"-" bson:"hashedPassword"`
}

// Users is a list of User
type Users []User

type UserMedia struct {
	Icon struct {
		UID    string `json:"uid" bson:"uid"`
		Source string `json:"source" bson:"source"`
	} `json:"icon,omitempty"`
	Banner struct {
		UID    string `json:"uid" bson:"uid"`
		Source string `json:"source" bson:"source"`
	} `json:"banner,omitempty" bson:"banner"`
}

// UserGameLibrary contains a list of Games a User collects
type UserGameLibrary struct {
	UserUID string                         `json:"userUID" bson:"userUID"`
	Library map[string]UserGameLibraryItem `json:"library" bson:"library"`
}

type UserGameLibraryItem struct {
	GameUID   string    `json:"gameUID" bson:"gameUID"`
	DateAdded time.Time `json:"dateAdded" bson:"dateAdded"`
}

// UserGamePublishHistory is the history of Games a User has published
type UserGamePublishHistory struct {
	UserUID string                                `json:"userUID" bson:"userUID"`
	History map[string]UserGamePublishHistoryItem `json:"history" bson:"history"`
}

type UserGamePublishHistoryItem struct {
	GameUID   string    `json:"gameUID" bson:"gameUID"`
	DateAdded time.Time `json:"dateAdded" bson:"dateAdded"`
}

// UserAppPublishHistory is the history of Apps a User has published
type UserAppPublishHistory struct {
	UserUID string                               `json:"userUID" bson:"userUID"`
	History map[string]UserAppPublishHistoryItem `json:"history" bson:"history"`
}

type UserAppPublishHistoryItem struct {
	AppUID    string    `json:"appUID" bson:"appUID"`
	DateAdded time.Time `json:"dateAdded" bson:"dateAdded"`
}

// UserGamePlayAccessHistory is the history of Games a User has access of playing
type UserGamePlayAccessHistory struct {
	UserUID string                                   `json:"userUID" bson:"userUID"`
	History map[string]UserGamePlayAccessHistoryItem `json:"history" bson:"history"`
}

type UserGamePlayAccessHistoryItem struct {
	GameUID   string         `json:"gameUID" bson:"gameUID"`
	Type      string         `json:"type" bson:"type"` // e.g. purchase, gift
	DateAdded time.Time      `json:"dateAdded" bson:"dateAdded"`
	Receipt   PaymentReceipt `json:"receipt" bson:"receipt"`
}

type PaymentReceipt struct {
	ID      string `json:"id" bson:"id"`
	Name    string `json:"name" bson:"name"`
	Service string `json:"service" bson:"service"`
	Amount  uint64 `json:"amount"`
}

const (
	UserStatusActive    = "active"    // email confirmed
	UserStatusPending   = "pending"   // email confirmation pending
	UserStatusSuspended = "suspended" // suspended
	UserStatusDeleted   = "deleted"   // deleted

	GamePlayAccessTypePurchase = "purchase"
	GamePlayAccessTypeGift     = "gift"
)

type UserProfile struct {
	UID      string `json:"uid"`
	Username string `json:"username"`
	Alias    string `json:"alias"`

	Description string    `json:"description"`
	Media       UserMedia `json:"media"`

	DateCreated time.Time `json:"dateCreated,omitempty"`
	LastOnline  time.Time `json:"lastOnline,omitempty"`

	Status string `json:"status,omitempty"`
	Email  string `json:"email,omitempty"`
}

func NewUserProfile(user User) UserProfile {
	profile := UserProfile{}
	profile.UID = user.UID
	profile.Username = user.Username
	profile.Alias = user.Alias
	profile.Description = user.Description
	profile.Media = user.Media
	profile.DateCreated = user.DateCreated
	profile.LastOnline = user.LastOnline
	profile.Status = user.Status
	profile.Email = user.Email
	return profile
}
