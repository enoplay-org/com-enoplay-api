package domain

import (
	"time"

	"gopkg.in/mgo.v2/bson"

	jwt "github.com/dgrijalva/jwt-go"
)

type SessionTokenRepository interface {
	VerifyAccess(tokenString string) (SessionTokenClaim, error)
	VerifyRefresh(tokenString string) (SessionTokenClaim, error)
	Create(sessionToken SessionTokenClaim) (string, error)
	DeleteRefresh(revoked RevokedSessionToken) error
}

type SessionTokenClaim struct {
	UserUID     string    `json:"userUID"`
	Type        string    `json:"type"`
	DateExpired time.Time `json:"dateExpired"`
	DateIssued  time.Time `json:"dateIssued"`
	JTI         string    `json:"jti"`
}

func NewRefreshToken(userUID string) SessionTokenClaim {
	return SessionTokenClaim{
		UserUID:     userUID,
		Type:        SessionTokenTypeRefresh,
		DateExpired: time.Now().Add(time.Hour * 24 * 7 * 4 * 3), // 3 months
	}

}

func NewAccessToken(userUID string) SessionTokenClaim {
	return SessionTokenClaim{
		UserUID:     userUID,
		Type:        SessionTokenTypeAccess,
		DateExpired: time.Now().Add(time.Hour * 24 * 3), // 3 days
	}
}

type RevokedSessionToken struct {
	ID          bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	DateExpired time.Time     `json:"dateExpired" bson:"dateExpired"`
	DateRevoked time.Time     `json:"dateRevoked" bson:"dateRevoked"`
}

type SessionToken struct {
	*jwt.Token
}

const (
	SessionTokenTypeAccess  = "access"
	SessionTokenTypeRefresh = "refresh"
)
