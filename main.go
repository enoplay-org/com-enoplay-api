package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/go-zoo/bone"
	"github.com/twinj/uuid"
	"github.com/urfave/negroni"
	"gitlab.com/enoplay/com-enoplay-api/infrastructure"
	"gitlab.com/enoplay/com-enoplay-api/interfaces/repository"
	"gitlab.com/enoplay/com-enoplay-api/interfaces/web"
	"gitlab.com/enoplay/com-enoplay-api/usecase"
)

// Environment Variables
const (
	envPort                = "PORT"
	envWWWHostURL          = "HOST_URL_WWW"
	envMobileHostURL       = "HOST_URL_MOBILE"
	envAppHostURL          = "HOST_URL_APP"
	envStaticHostURL       = "HOST_URL_STATIC"
	envPrivateSigningKey   = "PRIVATE_SIGNING_KEY"
	envPublicSigningKey    = "PUBLIC_SIGNING_KEY"
	envGrecaptchaSecretKey = "GRECAPTCHA_SECRET_KEY"
	envMongoDatabase       = "MONGO_DATABASE"
	envMongoFullURL        = "MONGO_FULLURI"
	envMailgunDomain       = "MAILGUN_DOMAIN"
	envMailgunPrivateKey   = "MAILGUN_PRIVATE_KEY"
	envMailgunPublicKey    = "MAILGUN_PUBLIC_KEY"
	envStripeSecretKey     = "STRIPE_SECRET_KEY"
)

func init() {
	// Initialize UUID generator
	uuid.Init()

	// Initialize global pseudo random generator
	rand.Seed(time.Now().UnixNano())

	// Initialize environment variables
	initEnvironmentVariables()
}

func main() {
	// Setup signing keys
	privateSigningKey, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(os.Getenv(envPrivateSigningKey)))
	if err != nil {
		log.Fatal("Error parsing private key")
		return
	}
	publicSigningKey, err := jwt.ParseRSAPublicKeyFromPEM([]byte(os.Getenv(envPublicSigningKey)))
	if err != nil {
		log.Fatal("Error parsing public key")
		return
	}

	// Setup Grecaptcha - Google's Recaptcha service
	grecaptchaOptions := &infrastructure.GrecaptchaOptions{
		SecretKey: os.Getenv(envGrecaptchaSecretKey),
	}
	grecaptchaHandler := infrastructure.NewGrecaptchaHandler(grecaptchaOptions)

	// Setup MongoDB - Database service
	mongoOptions := &infrastructure.MongoOptions{
		ServerName:   os.Getenv(envMongoFullURL),
		DatabaseName: os.Getenv(envMongoDatabase),
	}
	mongoHandler := infrastructure.NewMongoHandler(mongoOptions)
	mongoHandler.NewSession()

	handlers := make(map[string]repository.DbHandler)
	handlers["DbSessionTokenRepo"] = mongoHandler
	handlers["DbPlayTokenRepo"] = mongoHandler
	handlers["DbUserRepo"] = mongoHandler
	handlers["DbGameRepo"] = mongoHandler

	// Setup Mailgun - Email service
	mailgunOptions := infrastructure.MailgunOptions{
		Domain:        os.Getenv(envMailgunDomain),
		PrivateApiKey: os.Getenv(envMailgunPrivateKey),
		PublicApiKey:  os.Getenv(envMailgunPublicKey),
	}
	mailgunHandler := infrastructure.NewMailgunHandler(mailgunOptions)

	// Setup Stripe - Payment service
	stripeOptions := infrastructure.StripeOptions{
		SecretApiKey: os.Getenv(envStripeSecretKey),
	}
	stripeHandler := infrastructure.NewStripeHandler(stripeOptions)

	// Setup Enoplay App Api - App management service
	enoplayAppOptions := infrastructure.EnoplayAppOptions{
		ApiBase: os.Getenv(envAppHostURL),
	}
	enoplayAppHandler := infrastructure.NewEnoplayAppHandler(enoplayAppOptions)

	// Setup Enoplay Static Api - Image management service
	enoplayStaticOptions := infrastructure.EnoplayStaticOptions{
		ApiBase: os.Getenv(envStaticHostURL),
	}
	enoplayStaticHandler := infrastructure.NewEnoplayStaticHandler(enoplayStaticOptions)

	// Setup interactors & repositories
	sessionTokenInterator := usecase.NewSessionTokenInteractor()
	playTokenInteractor := usecase.NewPlayTokenInteractor()
	userInteractor := usecase.NewUserInteractor()
	gameInteractor := usecase.NewGameInteractor()

	tokenAuthorityOptions := repository.NewTokenAuthorityOptions(privateSigningKey, publicSigningKey)
	sessionTokenInterator.SessionTokenRepository = repository.NewDbSessionTokenRepo(handlers, tokenAuthorityOptions)
	playTokenInteractor.PlayTokenRepository = repository.NewDbPlayTokenRepo(handlers, tokenAuthorityOptions)
	userInteractor.UserRepository = repository.NewDbUserRepo(handlers, mailgunHandler, stripeHandler, enoplayStaticHandler, &repository.DbUserRepoOptions{
		WWWHostURL:    os.Getenv(envWWWHostURL),
		MobileHostURL: os.Getenv(envMobileHostURL),
	})
	gameInteractor.GameRepository = repository.NewDbGameRepo(handlers, enoplayAppHandler, enoplayStaticHandler)

	sessionTokenInterator.UserRepository = userInteractor.UserRepository
	playTokenInteractor.UserRepository = userInteractor.UserRepository
	userInteractor.GameRepository = gameInteractor.GameRepository
	gameInteractor.UserRepository = userInteractor.UserRepository
	playTokenInteractor.GameRepository = gameInteractor.GameRepository

	// Setup web handler
	w := web.NewHandler(&web.HandlerOptions{
		WwwHostURL:    envWWWHostURL,
		MobileHostURL: envMobileHostURL,
	}, grecaptchaHandler)
	w.SessionTokenInteractor = sessionTokenInterator
	w.PlayTokenInteractor = playTokenInteractor
	w.UserInteractor = userInteractor
	w.GameInteractor = gameInteractor

	// Setup route handling
	router := bone.New()
	n := negroni.New()
	n.UseFunc(w.ForceHTTPS)
	n.UseFunc(w.CORS)
	n.UseFunc(w.BlockIPList)

	router.Get("/", http.HandlerFunc(w.GetIndex))
	v0 := bone.New()
	v0.Get("/sessions", w.EnsureAuthentication(w.GetSessionTokenUser))                 // Get account information
	v0.Get("/sessions/token_access", w.EnsureAuthorization(w.VerifyAccessToken))       // Verify access token
	v0.Post("/sessions/token_refresh", w.CheckAuthentication(w.CreateRefreshToken))    // Login
	v0.Post("/sessions/token_access", w.EnsureAuthentication(w.CreateAccessToken))     // Renew access token
	v0.Delete("/sessions/token_refresh", w.EnsureAuthentication(w.DeleteRefreshToken)) // Logout

	v0.Get("/users", http.HandlerFunc(w.ListUsers))
	v0.Post("/users", http.HandlerFunc(w.CreateUser))                                                   // Register
	v0.Post("/users/email_confirmation_reset", w.EnsureAuthorization(w.ResetUserEmailConfirmationCode)) // Reset Email Confirmation code
	v0.Post("/users/email_confirmation", http.HandlerFunc(w.ConfirmUserEmail))                          // Confirm Email w/ code
	v0.Post("/users/password_reset", http.HandlerFunc(w.ResetUserPassword))                             // Reset Password request
	v0.Post("/users/password_update", http.HandlerFunc(w.UpdateUserPasswordWithCode))                   // Update Password w/ code
	v0.Post("/users/feedback", http.HandlerFunc(w.SendUserFeedback))

	v0.Get("/users/:username", http.HandlerFunc(w.GetUser))
	v0.Post("/users/:username/alias", w.EnsureAuthorization(w.UpdateUserAlias))
	v0.Post("/users/:username/username", w.EnsureAuthorization(w.UpdateUsername))
	v0.Post("/users/:username/email", w.EnsureAuthorization(w.UpdateUserEmail))
	v0.Post("/users/:username/description", w.EnsureAuthorization(w.UpdateUserDescription))
	v0.Post("/users/:username/password", w.EnsureAuthorization(w.UpdateUserPassword))

	v0.Delete("/users/:username", w.EnsureAuthorization(w.DeleteUser))

	v0.Get("/users/:username/game_publish_history", w.CheckAuthorization(w.GetUserGamePublishHistory))

	v0.Get("/users/game_play_access/:gid", http.HandlerFunc(w.VerifyAnonUserGamePlayAccess))
	v0.Get("/users/:username/game_play_access/:gid", w.EnsureAuthorization(w.VerifyUserGamePlayAccess))
	v0.Get("/users/:username/game_edit_access/:gid", w.EnsureAuthorization(w.VerifyUserGameEditAccess))
	v0.Post("/users/:username/game_purchase/:gid", w.EnsureAuthorization(w.PurchaseGame))

	v0.Post("/plays", w.EnsureAuthorization(w.CreatePlayToken))               // Create a playToken as an authenticated user
	v0.Post("/plays/anonymous", http.HandlerFunc(w.CreateAnonPlayToken))      // Create a playToken for anonymous user
	v0.Post("/plays/token_verification", http.HandlerFunc(w.VerifyPlayToken)) // Verify playToken

	v0.Get("/games", w.CheckAuthorization(w.ListGames))
	v0.Post("/games", w.EnsureAuthorization(w.CreateGame))
	v0.Get("/games/:gid", w.CheckAuthorization(w.GetGame))
	v0.Post("/games/:gid/title", w.EnsureAuthorization(w.UpdateGameTitle))
	v0.Post("/games/:gid/description", w.EnsureAuthorization(w.UpdateGameDescription))
	v0.Post("/games/:gid/price", w.EnsureAuthorization(w.UpdateGamePrice))
	v0.Post("/games/:gid/privacy", w.EnsureAuthorization(w.UpdateGamePrivacy))
	// v0.Post("/games/:gid/controls", w.EnsureAuthorization(w.UpdateGameControls))
	v0.Post("/games/:gid/system/browser", w.EnsureAuthorization(w.UpdateGameBrowserSupport))

	v0.Delete("/games/:gid", w.EnsureAuthorization(w.DeleteGame))

	// v0.Get("/.well-known/acme-challenge/ENfg7ya9gFfKac4PNzm78Wcptbne3_bhqLmRHRw7iNQ", http.HandlerFunc(w.GetCertBotKey)) // Certbot - Certificate verification

	router.SubRoute("/v0", v0)
	n.UseHandler(router)
	fmt.Printf("Running on port: %v\n", os.Getenv(envPort))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", os.Getenv(envPort)), n))
}

// IndexConfig represents the expected structure of /config/index.json
type IndexConfig struct {
	Port          string `json:"port"`
	WWWHostURL    string `json:"wwwHostURL"`
	MobileHostURL string `json:"mobileHostURL"`
	AppHostURL    string `json:"appHostURL"`
	StaticHostURL string `json:"staticHostURL"`
}

// GrecaptchaConfig represents the expected structure of /config/grecaptcha.json
type GrecaptchaConfig struct {
	SecretKey string `json:"secretKey"`
}

// MongoConfig represents the expected structure of /config/mongo.json
type MongoConfig struct {
	Database string `json:"database"`
	FullURI  string `json:"fullURI"`
}

// MailgunConfig represents the expected structure of /config/mailgun.json
type MailgunConfig struct {
	Domain     string `json:"domain"`
	PrivateKey string `json:"privateKey"`
	PublicKey  string `json:"publicKey"`
}

// StripeConfig represents the expected structure of /config/stripe.json
type StripeConfig struct {
	SecretKey string `json:"secretKey"`
}

func initEnvironmentVariables() {
	// Check if environment variables are provided (e.g. production)
	if len(os.Getenv(envPort)) != 0 {
		return
	}

	// Setup general settings
	indexFile, err := ioutil.ReadFile("config/index.json")
	if err != nil {
		log.Fatal(fmt.Sprintf("Error loading index config file %v", err.Error()))
	}
	var indexConfig IndexConfig
	err = json.Unmarshal(indexFile, &indexConfig)

	// Setup PORT
	port := os.Getenv(envPort)
	if len(port) == 0 {
		os.Setenv(envPort, indexConfig.Port)
	}

	// Setup wwwHostURL
	wwwHostURL := os.Getenv(envWWWHostURL)
	if len(wwwHostURL) == 0 {
		os.Setenv(envWWWHostURL, indexConfig.WWWHostURL)
	}

	// Setup mobileHostURL
	mobileHostURL := os.Getenv(envMobileHostURL)
	if len(mobileHostURL) == 0 {
		os.Setenv(envMobileHostURL, indexConfig.MobileHostURL)
	}

	// Setup appHostURL
	appHostURL := os.Getenv(envAppHostURL)
	if len(appHostURL) == 0 {
		os.Setenv(envAppHostURL, indexConfig.AppHostURL)
	}

	// Setup staticHostURL
	staticHostURL := os.Getenv(envStaticHostURL)
	if len(staticHostURL) == 0 {
		os.Setenv(envStaticHostURL, indexConfig.StaticHostURL)
	}

	// Setup Private Signing Key
	privateSigningKey := os.Getenv(envPrivateSigningKey)
	if len(privateSigningKey) == 0 {
		keyBytes, err2 := ioutil.ReadFile("config/keys/domain.key")
		if err2 != nil {
			panic(fmt.Errorf("Error loading private signing key: %v", err2.Error()))
		}
		os.Setenv(envPrivateSigningKey, string(keyBytes))
	}

	// Setup Public Signing Key
	publicSigningKey := os.Getenv(envPublicSigningKey)
	if len(publicSigningKey) == 0 {
		keyBytes, err2 := ioutil.ReadFile("config/keys/domain.key.pub")
		if err2 != nil {
			panic(fmt.Errorf("Error loading public signing key: %v", err2.Error()))
		}
		os.Setenv(envPublicSigningKey, string(keyBytes))
	}

	// Setup Grecaptcha (Secret Key)
	grecaptchaFile, err := ioutil.ReadFile("config/grecaptcha.json")
	if err != nil {
		log.Fatal(fmt.Sprintf("Error loading grecaptcha config file %v", err.Error()))
	}
	var grecaptchaConfig GrecaptchaConfig
	err = json.Unmarshal(grecaptchaFile, &grecaptchaConfig)

	// Secret Key
	grecaptchaSecretKey := os.Getenv(envGrecaptchaSecretKey)
	if len(grecaptchaSecretKey) == 0 {
		os.Setenv(envGrecaptchaSecretKey, grecaptchaConfig.SecretKey)
	}

	// Setup Mongo DB (Database and FullURI)
	mongoFile, err := ioutil.ReadFile("config/mongo.json")
	if err != nil {
		log.Fatal(fmt.Sprintf("Error loading mailgun config file %v", err.Error()))
	}
	var mongoConfig MongoConfig
	err = json.Unmarshal(mongoFile, &mongoConfig)

	// Mongo Database
	mongoDatabase := os.Getenv(envMongoDatabase)
	if len(mongoDatabase) == 0 {
		os.Setenv(envMongoDatabase, mongoConfig.Database)
	}

	// Mongo FullURI
	mongoFullURI := os.Getenv(envMongoFullURL)
	if len(mongoFullURI) == 0 {
		os.Setenv(envMongoFullURL, mongoConfig.FullURI)
	}

	// Setup Mailgun (Domain, Private Key and Public Key)
	mailgunFile, err := ioutil.ReadFile("config/mailgun.json")
	if err != nil {
		log.Fatal(fmt.Sprintf("Error loading mailgun config file %v", err.Error()))
	}
	var mailgunConfig MailgunConfig
	err = json.Unmarshal(mailgunFile, &mailgunConfig)

	// Mailgun Domain
	mailgunDomain := os.Getenv(envMailgunDomain)
	if len(mailgunDomain) == 0 {
		os.Setenv(envMailgunDomain, mailgunConfig.Domain)
	}

	// Mailgun Private Key
	mailgunPrivateKey := os.Getenv(envMailgunPrivateKey)
	if len(mailgunPrivateKey) == 0 {
		os.Setenv(envMailgunPrivateKey, mailgunConfig.PrivateKey)
	}

	// Mailgun Public Key
	mailgunPublicKey := os.Getenv(envMailgunPublicKey)
	if len(mailgunPublicKey) == 0 {
		os.Setenv(envMailgunPublicKey, mailgunConfig.PublicKey)
	}

	// Setup Stripe (Secret Key)
	stripeFile, err := ioutil.ReadFile("config/stripe.json")
	if err != nil {
		log.Fatal(fmt.Sprintf("Error loading mailgun config file %v", err.Error()))
	}
	var stripeConfig StripeConfig
	err = json.Unmarshal(stripeFile, &stripeConfig)

	// Stripe Secret Key
	stripeSecretKey := os.Getenv(envStripeSecretKey)
	if len(stripeSecretKey) == 0 {
		os.Setenv(envStripeSecretKey, stripeConfig.SecretKey)
	}
}
