package usecase

import "gitlab.com/enoplay/com-enoplay-api/domain"

// AddGameMigrateProperties updates the properties of a Game in case of system changes
func AddGameMigrateProperties(game domain.Game) domain.Game {

	// Add browser support
	if game.SystemSupport.Browser == nil {
		game = initGameSystemBrowserSupport(game)
	}

	return game
}
