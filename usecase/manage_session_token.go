package usecase

import (
	"fmt"

	"gopkg.in/mgo.v2/bson"

	"golang.org/x/crypto/bcrypt"

	"gitlab.com/enoplay/com-enoplay-api/domain"
)

type SessionTokenInteractor struct {
	SessionTokenRepository domain.SessionTokenRepository
	UserRepository         domain.UserRepository
}

func NewSessionTokenInteractor() *SessionTokenInteractor {
	return &SessionTokenInteractor{}
}

// VerifyAccess checks if an access token is valid
func (interactor *SessionTokenInteractor) VerifyAccess(token string) (string, error) {
	tokenClaim, err := interactor.SessionTokenRepository.VerifyAccess(token)
	if err != nil {
		return "", fmt.Errorf("Error verifying access token: %v", err)
	}

	return tokenClaim.UserUID, nil
}

// VerifyRefresh checks if a refresh token is valid
func (interactor *SessionTokenInteractor) VerifyRefresh(token string) (string, error) {
	tokenClaim, err := interactor.SessionTokenRepository.VerifyRefresh(token)
	if err != nil {
		return "", fmt.Errorf("Error verifying refresh token: %v", err)
	}

	return tokenClaim.UserUID, nil
}

// CreateAccess generates an access token from a refresh token
func (interactor *SessionTokenInteractor) CreateAccess(refreshToken string) (string, error) {
	tokenClaim, err := interactor.SessionTokenRepository.VerifyRefresh(refreshToken)
	if err != nil {
		return "", fmt.Errorf("Error verifying refresh token: %v", err)
	}

	sessionTokenClaim := domain.NewAccessToken(tokenClaim.UserUID)
	sessionToken, err := interactor.SessionTokenRepository.Create(sessionTokenClaim)
	if err != nil {
		return "", fmt.Errorf("Error creating access token: %v", err)
	}

	return sessionToken, nil
}

// CreateRefresh generates a refresh token
func (interactor *SessionTokenInteractor) CreateRefresh(usernameOrEmail, password string) (string, domain.User, error) {
	var user domain.User

	if isValidEmail(usernameOrEmail) {
		user, _ = interactor.UserRepository.FindByEmail(usernameOrEmail)
	}

	if uMinLength, uMaxLength, uAlphaNum, uNonVanity := isValidUsername(usernameOrEmail); (uMinLength && uMaxLength && uAlphaNum && uNonVanity) && user.UID == "" {
		user, _ = interactor.UserRepository.GetByUsername(usernameOrEmail)
	}

	// Ensure credentials are valid
	if !hasValidPassword(user.HashedPassword, password) {
		return "", domain.User{}, fmt.Errorf("Invalid credentials")
	}

	sessionTokenClaim := domain.NewRefreshToken(user.UID)
	sessionToken, err := interactor.SessionTokenRepository.Create(sessionTokenClaim)
	if err != nil {
		return "", domain.User{}, fmt.Errorf("Token could not be generated: %v", err)
	}

	return sessionToken, user, nil
}

// DeleteRefresh deletes a refresh token
func (interactor *SessionTokenInteractor) DeleteRefresh(token string) error {
	tokenClaim, err := interactor.SessionTokenRepository.VerifyRefresh(token)
	if err != nil {
		return err
	}

	revoked := domain.RevokedSessionToken{
		ID:          bson.ObjectIdHex(tokenClaim.JTI),
		DateExpired: tokenClaim.DateExpired,
	}

	return interactor.SessionTokenRepository.DeleteRefresh(revoked)
}

func hasValidPassword(hashedPassword, password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))

	if err != nil {
		return false
	}

	return true
}
