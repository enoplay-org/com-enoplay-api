package usecase

import (
	"fmt"
	"math/rand"

	"gitlab.com/enoplay/com-enoplay-api/domain"
)

type GameInteractor struct {
	GameRepository domain.GameRepository
	UserRepository domain.UserRepository
}

func NewGameInteractor() *GameInteractor {
	return &GameInteractor{}
}

func (interactor *GameInteractor) Get(uid string) (domain.Game, error) {
	game, err := interactor.GameRepository.Get(uid)
	game = AddGameMigrateProperties(game)
	return game, err
}

func (interactor *GameInteractor) GetByGID(gid string) (domain.Game, error) {
	game, err := interactor.GameRepository.GetByGID(gid)
	game = AddGameMigrateProperties(game)
	return game, err
}

func (interactor *GameInteractor) ListGames(user domain.User, queryList map[string]interface{}, limit int, sort string) domain.Games {
	games := interactor.GameRepository.Filter(queryList, limit, sort)

	// Remove private games that the user didn't publish
	removeList := []int{}
	for i, game := range games {
		if game.Privacy == domain.GamePrivacyPrivate {
			if (user.UID != "") && (game.Publisher.UID == user.UID) {
				continue
			} else {
				removeList = append(removeList, i)
			}
		}
	}

	for i := range removeList {
		games = append(games[:i], games[i+1:]...)
	}

	// Add migration properties
	for i, game := range games {
		games[i] = AddGameMigrateProperties(game)
	}

	return games
}

const MaxGameUploadPerUser = 4

// Create creates a new Game
func (interactor *GameInteractor) Create(user domain.User, game domain.Game) (domain.Game, error) {
	// Confirm that user has permission
	if user.Status != domain.UserStatusActive {
		return domain.Game{}, fmt.Errorf("Access denied. User status is currently %v", user.Status)
	}

	userGamePublishHistory, err := interactor.UserRepository.GetGamePublishHistory(user.UID)
	if err != nil {
		userGamePublishHistory.History = make(map[string]domain.UserGamePublishHistoryItem)
	}

	if len(userGamePublishHistory.History) >= MaxGameUploadPerUser {
		return domain.Game{}, fmt.Errorf("A maximum of %v games can be uploaded at this time", MaxGameUploadPerUser)
	}

	// Validate required fields
	if !isValidGameTitle(game.Title) {
		return domain.Game{}, fmt.Errorf("Invalid Title")
	}

	if !isValidGameDescription(game.Description) {
		return domain.Game{}, fmt.Errorf("Invalid Description")
	}

	if !game.Price.Valid() {
		return domain.Game{}, fmt.Errorf("Invalid Price Tier")
	}

	game.GID = interactor.reserveGID()
	game.Publisher.UID = user.UID
	game.Publisher.Username = user.Username
	game.Status = domain.GameStatusInactive
	game.Privacy = domain.GamePrivacyUnlisted

	game = initGameSystemBrowserSupport(game)

	game, err = interactor.GameRepository.Create(game)
	if err != nil {
		return domain.Game{}, fmt.Errorf(err.Error())
	}

	gamePublishItem := domain.UserGamePublishHistoryItem{
		GameUID: game.UID,
	}

	err = interactor.UserRepository.PublishGame(user.UID, gamePublishItem)
	return game, err
}

func (interactor *GameInteractor) UpdateTitle(game domain.Game, title string) (domain.Game, error) {
	if !isValidGameTitle(title) {
		return domain.Game{}, fmt.Errorf("Error: Invalid game title")
	}

	game.Title = title

	return interactor.GameRepository.UpdateTitle(game)
}

func (interactor *GameInteractor) UpdateDescription(game domain.Game, description string) (domain.Game, error) {
	if !isValidGameDescription(description) {
		return domain.Game{}, fmt.Errorf("Error: Invalid game description")
	}

	game.Description = description

	return interactor.GameRepository.UpdateDescription(game)
}

func (interactor *GameInteractor) UpdatePrice(game domain.Game, price domain.GamePrice) (domain.Game, error) {
	if !price.Valid() {
		return domain.Game{}, fmt.Errorf("Error: Invalid game price")
	}

	game.Price = price

	return interactor.GameRepository.UpdatePrice(game)
}

func (interactor *GameInteractor) UpdatePrivacy(game domain.Game, privacy string) (domain.Game, error) {
	if !isValidGamePrivacy(privacy) {
		return domain.Game{}, fmt.Errorf("Error: Invalid game privacy")
	}

	game.Privacy = privacy

	return interactor.GameRepository.UpdatePrivacy(game)
}

func (interactor *GameInteractor) UpdateBrowserSupport(game domain.Game, support domain.GameBrowserSupport) (domain.Game, error) {
	// Ensure browsers are valid
	for browser := range support {
		if !isValidGameBrowser(browser) {
			return domain.Game{}, fmt.Errorf("Error: Invalid game browser listed: %v", browser)
		}
	}

	// Update browser support
	for browser := range support {
		// Setup mobile
		if support[browser].Mobile == true || support[browser].Mobile == false {
			game.SystemSupport.Browser[browser] = domain.GameBrowserSupportItem{
				Mobile:  support[browser].Mobile,
				Desktop: game.SystemSupport.Browser[browser].Desktop,
			}
		}

		// Setup desktop
		if support[browser].Desktop == true || support[browser].Desktop == false {
			game.SystemSupport.Browser[browser] = domain.GameBrowserSupportItem{
				Mobile:  game.SystemSupport.Browser[browser].Mobile,
				Desktop: support[browser].Desktop,
			}
		}
	}

	return interactor.GameRepository.UpdateSystemBrowserSupport(game)
}

func (interactor *GameInteractor) Delete(sessionToken string, game domain.Game) error {

	// Delete Game
	err := interactor.GameRepository.Delete(sessionToken, game)
	if err != nil {
		return fmt.Errorf("Error deleting game: %v", err)
	}

	// Remove Game from User publish history
	interactor.UserRepository.UnpublishGame(game.Publisher.UID, game.UID)

	return nil
}

// TransferTo transfers the game to another User
func (interactor *GameInteractor) TransferTo() {

}

func (interactor *GameInteractor) reserveGID() string {
	gid := generateGID()

	for {
		if !interactor.GameRepository.ExistsByGID(gid) {
			break
		}
		// get a new GID
		gid = generateGID()
	}

	return gid
}

const maxGIDLength = 11
const gidCharacters = "01234556789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-"

func generateGID() string {
	gid := []byte("")

	for i := 0; i < maxGIDLength; i++ {
		gid = append(gid, gidCharacters[rand.Intn(len(gidCharacters))])
	}

	return string(gid)
}

func isValidGameTitle(title string) bool {
	return title != ""
}

func isValidGameDescription(description string) bool {
	return description != ""
}

func isValidGamePrivacy(privacy string) bool {
	switch privacy {
	case domain.GamePrivacyPrivate, domain.GamePrivacyPublic, domain.GamePrivacyUnlisted:
		return true
	}
	return false
}

func isValidGameBrowser(browser string) bool {
	switch browser {
	case domain.GameBrowserChrome, domain.GameBrowserFirefox,
		domain.GameBrowserSafari, domain.GameBrowserEdge, domain.GameBrowserIE:
		return true
	default:
		return false
	}
}

func initGameSystemBrowserSupport(game domain.Game) domain.Game {
	game.SystemSupport.Browser = make(map[string]domain.GameBrowserSupportItem)

	// Assume all browsers are supported by default
	support := domain.GameBrowserSupportItem{
		Mobile:  true,
		Desktop: true,
	}

	// Setup Chrome support
	game.SystemSupport.Browser[domain.GameBrowserChrome] = support

	// Setup Firefox support
	game.SystemSupport.Browser[domain.GameBrowserFirefox] = support

	// Setup Safari support
	game.SystemSupport.Browser[domain.GameBrowserSafari] = support

	// Setup Edge support
	game.SystemSupport.Browser[domain.GameBrowserEdge] = support

	// Setup IE support
	game.SystemSupport.Browser[domain.GameBrowserIE] = support

	return game
}
